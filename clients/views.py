from django.http import HttpResponse
from django.core import serializers
from django.shortcuts import render
import json
from django.http import JsonResponse

from simple_rest import Resource
from django.core.serializers.json import DjangoJSONEncoder

from .models import Client

def index(request):
    return render(request, 'index.html')


class Clients(Resource):

    def get(self, request):
        pageSize = request.GET.get('pageSize', '')
        pageIndex = request.GET.get('pageIndex', '')

        # we need to create logic based on pageSize and total records and call SQL to return the records we need to show on current page
        queryset = Client.objects.filter(pk__lte=pageSize).values()
        print queryset
        return JsonResponse({"data": list(queryset),"itemsCount":6})

        response_data = {}
        response_data['data'] = clients
        response_data['itemsCount'] = 6 #This will be total records e.g. all clients, i just hard-coded it to 6
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def post(self, request):
        Client.objects.create(
            name = request.POST.get("name"),
            age = request.POST.get("age"),
            address = request.POST.get("address"),
            married =  True if request.POST.get("married") == 'true' else False
        )
        return HttpResponse(status = 201)

    def put(self, request, client_id):
        client = Client.objects.get(pk = client_id)
        client.name = request.PUT.get("name")
        client.age = request.PUT.get("age")
        client.address = request.PUT.get("address")
        client.married = True if request.PUT.get("married") == 'true' else False
        client.save()
        return HttpResponse(status = 200)

    def delete(self, request, client_id):
        client = Client.objects.get(pk = client_id)
        client.delete()
        return HttpResponse(status = 200)

    def to_json(self, objects):
        return serializers.serialize('json', objects)
